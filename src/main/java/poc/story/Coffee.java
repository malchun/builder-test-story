package poc.story;

import lombok.Builder;

@Builder
public class Coffee {
    private int espresso;
    private int water;
    private int syrup;
    private int milk;
    private boolean cardamom;
    private boolean cinnamon;

    public Coffee(int espresso, int water, int syrup, int milk, boolean cardamom, boolean cinnamon) {
        this.espresso = espresso;
        this.water = water;
        this.syrup = syrup;
        this.milk = milk;
        this.cardamom = cardamom;
        this.cinnamon = cinnamon;
    }

    public String makeCoffee()
    {
        return "Making coffee with " + espresso + " espresso, " + water + " water, " + syrup + " syrup, " + milk + " milk" + (cardamom ? ", cardamom" : "") + (cinnamon ? ", cinnamon" : "");
    }
}
