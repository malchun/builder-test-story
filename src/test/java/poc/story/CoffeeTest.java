package poc.story;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CoffeeTest {

    @ParameterizedTest
    @MethodSource("coffeeBuilderProvider")
    public void testWithBuilder(String expectedResult, Coffee.CoffeeBuilder coffeeBuilder) {
        Coffee coffee = coffeeBuilder.build();
        assertEquals(expectedResult, coffee.makeCoffee());
    }

    public static Stream<Arguments> coffeeBuilderProvider() {
        return Stream.of(
                Arguments.of("Making coffee with 2 espresso, 4 water, 1 syrup, 3 milk, cardamom, cinnamon",
                        Coffee.builder()
                                .espresso(2)
                                .water(4)
                                .syrup(1)
                                .milk(3)
                                .cardamom(true)
                                .cinnamon(true)
                ),
                Arguments.of("Making coffee with 3 espresso, 5 water, 2 syrup, 4 milk",
                        Coffee.builder()
                                .espresso(3)
                                .water(5)
                                .syrup(2)
                                .milk(4)
                                .cardamom(false)
                                .cinnamon(false)
                )
        );
    }

}
